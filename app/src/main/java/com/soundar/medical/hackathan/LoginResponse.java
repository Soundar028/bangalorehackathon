package com.soundar.medical.hackathan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.soundar.medical.hackathan.baseclasses.PojoUtils;

public class LoginResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message = null;

    public String getMessage() {
        return PojoUtils.checkResult(message);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_code_txt")
        @Expose
        private String userCodeTxt;
        @SerializedName("user_salutation")
        @Expose
        private String userSalutation;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("user_mobile")
        @Expose
        private String userMobile;
        @SerializedName("user_last_name")
        @Expose
        private Object userLastName;
        @SerializedName("user_category")
        @Expose
        private Object userCategory;
        @SerializedName("facebook_id_txt")
        @Expose
        private Object facebookIdTxt;
        @SerializedName("google_id_txt")
        @Expose
        private Object googleIdTxt;
        @SerializedName("user_gender")
        @Expose
        private String userGender;
        @SerializedName("user_dob")
        @Expose
        private Object userDob;
        @SerializedName("user_image")
        @Expose
        private Object userImage;
        @SerializedName("user_address")
        @Expose
        private Object userAddress;
        @SerializedName("user_city")
        @Expose
        private Object userCity;
        @SerializedName("user_state")
        @Expose
        private Object userState;
        @SerializedName("user_zip")
        @Expose
        private Object userZip;
        @SerializedName("user_country")
        @Expose
        private Object userCountry;
        @SerializedName("user_college")
        @Expose
        private Object userCollege;
        @SerializedName("user_mobile_verify")
        @Expose
        private String userMobileVerify;
        @SerializedName("otp_no")
        @Expose
        private Object otpNo;
        @SerializedName("user_email_verify")
        @Expose
        private String userEmailVerify;
        @SerializedName("email_verified_at")
        @Expose
        private Object emailVerifiedAt;
        @SerializedName("user_subscribe")
        @Expose
        private String userSubscribe;
        @SerializedName("user_online_status")
        @Expose
        private String userOnlineStatus;
        @SerializedName("user_register_type")
        @Expose
        private String userRegisterType;
        @SerializedName("user_register_platform")
        @Expose
        private String userRegisterPlatform;
        @SerializedName("user_status")
        @Expose
        private String userStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUserCodeTxt() {
            return userCodeTxt;
        }

        public void setUserCodeTxt(String userCodeTxt) {
            this.userCodeTxt = userCodeTxt;
        }

        public String getUserSalutation() {
            return userSalutation;
        }

        public void setUserSalutation(String userSalutation) {
            this.userSalutation = userSalutation;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserMobile() {
            return userMobile;
        }

        public void setUserMobile(String userMobile) {
            this.userMobile = userMobile;
        }

        public Object getUserLastName() {
            return userLastName;
        }

        public void setUserLastName(Object userLastName) {
            this.userLastName = userLastName;
        }

        public Object getUserCategory() {
            return userCategory;
        }

        public void setUserCategory(Object userCategory) {
            this.userCategory = userCategory;
        }

        public Object getFacebookIdTxt() {
            return facebookIdTxt;
        }

        public void setFacebookIdTxt(Object facebookIdTxt) {
            this.facebookIdTxt = facebookIdTxt;
        }

        public Object getGoogleIdTxt() {
            return googleIdTxt;
        }

        public void setGoogleIdTxt(Object googleIdTxt) {
            this.googleIdTxt = googleIdTxt;
        }

        public String getUserGender() {
            return userGender;
        }

        public void setUserGender(String userGender) {
            this.userGender = userGender;
        }

        public Object getUserDob() {
            return userDob;
        }

        public void setUserDob(Object userDob) {
            this.userDob = userDob;
        }

        public Object getUserImage() {
            return userImage;
        }

        public void setUserImage(Object userImage) {
            this.userImage = userImage;
        }

        public Object getUserAddress() {
            return userAddress;
        }

        public void setUserAddress(Object userAddress) {
            this.userAddress = userAddress;
        }

        public Object getUserCity() {
            return userCity;
        }

        public void setUserCity(Object userCity) {
            this.userCity = userCity;
        }

        public Object getUserState() {
            return userState;
        }

        public void setUserState(Object userState) {
            this.userState = userState;
        }

        public Object getUserZip() {
            return userZip;
        }

        public void setUserZip(Object userZip) {
            this.userZip = userZip;
        }

        public Object getUserCountry() {
            return userCountry;
        }

        public void setUserCountry(Object userCountry) {
            this.userCountry = userCountry;
        }

        public Object getUserCollege() {
            return userCollege;
        }

        public void setUserCollege(Object userCollege) {
            this.userCollege = userCollege;
        }

        public String getUserMobileVerify() {
            return userMobileVerify;
        }

        public void setUserMobileVerify(String userMobileVerify) {
            this.userMobileVerify = userMobileVerify;
        }

        public Object getOtpNo() {
            return otpNo;
        }

        public void setOtpNo(Object otpNo) {
            this.otpNo = otpNo;
        }

        public String getUserEmailVerify() {
            return userEmailVerify;
        }

        public void setUserEmailVerify(String userEmailVerify) {
            this.userEmailVerify = userEmailVerify;
        }

        public Object getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(Object emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public String getUserSubscribe() {
            return userSubscribe;
        }

        public void setUserSubscribe(String userSubscribe) {
            this.userSubscribe = userSubscribe;
        }

        public String getUserOnlineStatus() {
            return userOnlineStatus;
        }

        public void setUserOnlineStatus(String userOnlineStatus) {
            this.userOnlineStatus = userOnlineStatus;
        }

        public String getUserRegisterType() {
            return userRegisterType;
        }

        public void setUserRegisterType(String userRegisterType) {
            this.userRegisterType = userRegisterType;
        }

        public String getUserRegisterPlatform() {
            return userRegisterPlatform;
        }

        public void setUserRegisterPlatform(String userRegisterPlatform) {
            this.userRegisterPlatform = userRegisterPlatform;
        }

        public String getUserStatus() {
            return userStatus;
        }

        public void setUserStatus(String userStatus) {
            this.userStatus = userStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }
}
