package com.soundar.medical.hackathan.baseclasses;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.soundar.medical.hackathan.baseinterface.DataCallback;
import com.soundar.medical.hackathan.baseinterface.RestApiService;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class RestDataHelper {
    private final Context context;
    Call<String> request;
    private Calendar calendar;

    public RestDataHelper(Context context) {
        this.context = context;
        calendar = Calendar.getInstance();
    }

    private static OkHttpClient getClient() {
        return new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).readTimeout(20, TimeUnit.SECONDS).writeTimeout(20, TimeUnit.SECONDS).build();
    }

    private static Gson getGson() {
        return new GsonBuilder().setLenient().excludeFieldsWithoutExposeAnnotation().create();
    }

    private void debugUrlFormation(String[] urlPathData, JSONObject jsonObject) throws Exception {

        StringBuilder sb = new StringBuilder();
        sb.append(PreferenceManager.getString(TextConstant.APP_DOMAIN, ""));
        if (urlPathData.length > 0)
            sb.append(urlPathData[0]);
        if (urlPathData.length > 1)
            sb.append("/").append(urlPathData[1]);
        if (urlPathData.length > 2)
            sb.append("/").append(urlPathData[2]);
        sb.append("?");
        Iterator<String> keys = jsonObject.keys();
        if (keys.hasNext()) {
            String key = keys.next();
            sb.append(key);
            sb.append("=");
            sb.append(jsonObject.getString(key));
        }
        while (keys.hasNext()) {
            sb.append("&");
            String key = keys.next();
            sb.append(key);
            sb.append("=");
            sb.append(jsonObject.getString(key));
        }

        NativeUtils.ErrorLog("Normal URL", sb.toString());
    }


    private String encodeDecode(String encryptedData) {
        String encodeDecodeData = "";
        try {
            encodeDecodeData = URLEncoder.encode(encryptedData, "UTF-8");
            encodeDecodeData = URLDecoder.decode(encodeDecodeData, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            encodeDecodeData = "";
            e.printStackTrace();
        }
        return encodeDecodeData;
    }

    private RestApiService getRetrofitApi() {
        return new Retrofit.Builder().client(getClient()).baseUrl(PreferenceManager.getString(TextConstant.APP_DOMAIN, "")).addConverterFactory(ScalarsConverterFactory.create()).addConverterFactory(GsonConverterFactory.create(getGson())).build().create(RestApiService.class);
    }

    private void HeptagonRetrofitApiCallBack(Call<String> request, final DataCallback heptagonDataCallback, final String imageUploadPath) {
        request.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                calendar = Calendar.getInstance();
                if (response.isSuccessful()) {

                    try {
                        String result = response.body();
                        NativeUtils.ErrorLog("Result", result);
                        heptagonDataCallback.onSuccess(result);

                    } catch (Exception e) {
                        NativeUtils.ErrorLog("Exception", e.getMessage());
                        if (!imageUploadPath.equals("NO"))
                            NativeUtils.successNoAlert(context);
                        heptagonDataCallback.onFailure("", "");
                    }
                } else {
                    String result = "";
                    CommonErrorResult errorResult;
                    String errorcode = "" + response.code();
                    String errorMessage = "";
                    switch (response.code()) {
                        case 400:
                        case 401:
                        case 409:
                            try {
                                result = response.errorBody() != null ? response.errorBody().string() : "";
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            try {
                                errorResult = new Gson().fromJson(NativeUtils.getJsonReader(result), CommonErrorResult.class);
                                if (errorResult != null) {
                                    if (response.code() != 409 && !imageUploadPath.equals("NO"))
                                        NativeUtils.commonHepAlert(context, errorResult.getError(), true);
                                    errorMessage = errorResult.getError();
                                } else {
                                    result = response.message();
                                    if (!imageUploadPath.equals("NO"))
                                        NativeUtils.commonHepAlert(context, result, true);
                                }
                            } catch (Exception e) {
                                NativeUtils.ErrorLog("Exception", e.getMessage());
                                if (!imageUploadPath.equals("NO"))
                                    NativeUtils.successNoAlert(context);
                            }
                            break;
                        default:
                            if (!imageUploadPath.equals("NO"))
                                NativeUtils.commonHepAlert(context, response.message(), true);
                            break;
                    }
                    NativeUtils.ErrorLog("ErrorResponse", "" + response.code() + " " + response.message());
                    NativeUtils.ErrorLog("ErrorMessage", "" + errorMessage);
                    NativeUtils.ErrorLog("ErrorResult", "" + result);
                    heptagonDataCallback.onFailure(errorcode, errorMessage);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                calendar = Calendar.getInstance();
                NativeUtils.ErrorLog("onFailure", t.getMessage());
                if (!imageUploadPath.equals("NO"))
                    NativeUtils.successNoAlert(context);
                heptagonDataCallback.onFailure("", "");
            }
        });
    }

    public void postEnData(String[] urlPathData, JSONObject jsonObject, DataCallback heptagonDataCallback) throws Exception {
        debugUrlFormation(urlPathData, jsonObject);

        if (jsonObject.has("STOP"))
            return;

        String alertKey = "";
        if (jsonObject.has("NO_ALERT"))
            alertKey = "NO";

        Iterator<String> keys = jsonObject.keys();
        Map<String, String> params = new HashMap<>();
        if (keys.hasNext()) {
            String key = keys.next();
            params.put(key, jsonObject.getString(key));
        }
        while (keys.hasNext()) {
            String key = keys.next();
            params.put(key, jsonObject.getString(key));
        }


        request = getRetrofitApi().commonEnPost(urlPathData[0], params);

        HeptagonRetrofitApiCallBack(request, heptagonDataCallback, alertKey);
    }


    public void setCancel() {
        if (request != null)
            request.cancel();
    }

    @SuppressWarnings("SameReturnValue")
    public boolean getCancelStatus() {
        if (request != null)
            request.isCanceled();
        return false;
    }


}
