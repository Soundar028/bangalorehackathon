package com.soundar.medical.hackathan.baseclasses;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.soundar.medical.hackathan.R;


public class ProgressDialog {
    private static Dialog progressDialogObj;

    public static Dialog showLoader(Context context, boolean cancelFlag) {
        if (progressDialogObj == null) {
            progressDialogObj = new Dialog(context);
            progressDialogObj.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialogObj.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialogObj.setCancelable(cancelFlag);
            progressDialogObj.setContentView(R.layout.dialog_progress);
        }

        if (!progressDialogObj.isShowing()) {
            progressDialogObj.show();
        }
        return progressDialogObj;
    }

    public static void dismissLoader(Dialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialogObj = null;
        }
    }
}
