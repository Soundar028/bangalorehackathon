package com.soundar.medical.hackathan.baseclasses;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.soundar.medical.hackathan.R;
import com.soundar.medical.hackathan.baseinterface.NativeDialogClickListener;

import java.io.StringReader;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

public class NativeUtils {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;


    public static void ErrorLog(String key, String value) {
        int maxLength = 1000;
            if (value != null)
                for (int i = 0; i < value.length(); i += maxLength)
                    Log.e(key, value.substring(i, Math.min(value.length(), i + maxLength)));
    }


    public static void noInternetAlert(Activity activity) {
        commonHepAlert(activity, activity.getString(R.string.no_internet_alert), false);
    }

    public static void successNoAlert(Context activity) {
        commonHepAlert(activity, activity.getString(R.string.something_went_wrong), true);
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void callNativeAlert(Context activity, View view, String title, String message, boolean cancelableFlag, String positiveButtonText, String negativeButtonText, final NativeDialogClickListener nativeDialogClickListener) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            if (view != null) {
                builder.setView(view);
            } else {
                builder.setTitle(title).setMessage(Html.fromHtml(message));
            }

            builder.setCancelable(cancelableFlag).setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    nativeDialogClickListener.onPositive(dialog);
                }
            });
            if (!negativeButtonText.equals("")) {
                builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        nativeDialogClickListener.onNegative(dialog);
                    }
                });
            }
            builder.create().show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static JsonReader getJsonReader(String data) {
        JsonReader reader = new JsonReader(new StringReader(data));
        reader.setLenient(true);
        return reader;
    }


    public static boolean checkPatternValidate(String pattern, String text) {
        return Pattern.matches(pattern, text);
    }

    public static String getText(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static boolean isEmptyText(EditText editText) {
        return editText.getText().toString().trim().isEmpty();
    }

    public static boolean isEmptyText(String value) {
        if (value == null)
            return true;
        return value.trim().isEmpty();
    }

    public static <T> T jsonToPojoParser(String data, Class<T> className) {
        return new Gson().fromJson(NativeUtils.getJsonReader(data), className);
    }

    public static <T> List<T> jsonToListParser(String data, Class<T[]> tClass) {
        T[] arr = new Gson().fromJson(data, tClass);
        return Arrays.asList(arr);
    }


    public static String setTimeFormatAMPM(int hourOfDay, int minute) {
        String format;

        if (hourOfDay == 0) {
            hourOfDay += 12;
            format = "AM";
        } else if (hourOfDay == 12) {
            format = "PM";
        } else if (hourOfDay > 12) {
            hourOfDay -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        return hourOfDay + " : " + minute + " " + format;
    }


    public static void commonHepAlert(Context context, String alertText, boolean isErrorAlert) {
        if (alertText != null && !alertText.equals(""))
            if (TextConstant.ALERT_FLAG) {
                View view = getAlertDialogView(context, isErrorAlert, alertText);

                callNativeAlert(context, view, "", alertText, true, context.getString(R.string.alert_dialog_ok), "", new NativeDialogClickListener() {
                    @Override
                    public void onPositive(DialogInterface dialog) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegative(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, alertText, Toast.LENGTH_SHORT).show();
            }
    }

    public static void commonHepAlertCallBack(Context context, String alertText, boolean isErrorAlert, final NativeDialogClickListener nativeDialogClickListener) {
        if (alertText != null && !alertText.equals(""))
            if (TextConstant.ALERT_FLAG) {
                View view = getAlertDialogView(context, isErrorAlert, alertText);

                callNativeAlert(context, view, "", "", false, context.getString(R.string.alert_dialog_ok), "", new NativeDialogClickListener() {
                    @Override
                    public void onPositive(DialogInterface dialog) {
                        dialog.dismiss();
                        nativeDialogClickListener.onPositive(dialog);
                    }

                    @Override
                    public void onNegative(DialogInterface dialog) {
                        dialog.dismiss();
                        nativeDialogClickListener.onNegative(dialog);
                    }
                });
            } else {
                Toast.makeText(context, alertText, Toast.LENGTH_SHORT).show();
            }
        else
            nativeDialogClickListener.onPositive(null);
    }

    private static View getAlertDialogView(Context context, boolean isErrorView, String alertText) {
        Calendar calendar = Calendar.getInstance();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_alert_details, null);
        LinearLayout ll_error = dialogView.findViewById(R.id.ll_error);
        LinearLayout ll_company_code = dialogView.findViewById(R.id.ll_company_code);
        LinearLayout ll_emp_code = dialogView.findViewById(R.id.ll_emp_code);
        TextView tv_date = dialogView.findViewById(R.id.tv_date);
        TextView tv_company_code = dialogView.findViewById(R.id.tv_company_code);
        TextView tv_emp_code = dialogView.findViewById(R.id.tv_emp_code);
        TextView tv_alert_text = dialogView.findViewById(R.id.tv_alert_text);
        tv_alert_text.setText(alertText);

        tv_alert_text.setTextColor(getColor(context, R.color.black));

        return dialogView;
    }

    private static int getColor(Context context, int color) {
        return ContextCompat.getColor(context, color);
    }
}
