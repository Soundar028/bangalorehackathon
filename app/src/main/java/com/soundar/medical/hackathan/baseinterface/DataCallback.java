package com.soundar.medical.hackathan.baseinterface;

public interface DataCallback {
    void onSuccess(String data);

    void onFailure(String error, String message);
}
