package com.soundar.medical.hackathan.baseclasses;

public class TextConstant {


    public static final String PREF_NAME = "hackathan";

    public static final boolean ALERT_FLAG = true;

    public static final String APP_DOMAIN = PREF_NAME + "app_domain";

    public static final String APP_TEXT_SEND = "voice_process.php";

}
