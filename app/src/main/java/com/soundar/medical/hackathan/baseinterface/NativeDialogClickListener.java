package com.soundar.medical.hackathan.baseinterface;

import android.content.DialogInterface;

public interface NativeDialogClickListener
{
    void onPositive(DialogInterface dialog);

    void onNegative(DialogInterface dialog);
}
