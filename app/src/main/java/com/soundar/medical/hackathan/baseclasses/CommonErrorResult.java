package com.soundar.medical.hackathan.baseclasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CommonErrorResult {
    @SerializedName("error")
    @Expose
    private String error;

    public String getError() {
        return PojoUtils.checkResult(error);
    }

    public void setError(String error) {
        this.error = error;
    }
}
