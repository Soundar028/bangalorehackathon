package com.soundar.medical.hackathan.baseactivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.soundar.medical.hackathan.baseclasses.NativeUtils;
import com.soundar.medical.hackathan.baseclasses.NetworkConnectivity;
import com.soundar.medical.hackathan.baseclasses.ProgressDialog;
import com.soundar.medical.hackathan.baseclasses.RestDataHelper;
import com.soundar.medical.hackathan.baseinterface.DataCallback;

import org.json.JSONObject;

public abstract class BaseActivity extends AppCompatActivity {
    private static BaseActivity inst;
    private final String NO_INTERNET = "no_internet";
    private final int REQUEST_CHECK_SETTINGS = 1004;
    Context context = BaseActivity.this;
    Dialog heptagonProgressDialog;

    public static BaseActivity instance() {
        return inst;
    }

    public abstract void onSuccessResponse(String key, String data);

    public abstract void onFailureResponse(String key, String error);

    protected abstract void initViews();

    protected final void onCreate(Bundle savedInstanceState, int layoutResID) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResID);

        initResViews();
    }

    private void initResViews() {
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (inst == null)
            inst = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (inst != null)
            inst = null;
    }

    public boolean checkInternet(String urlPath, boolean offlineFlag) {
        if (!NetworkConnectivity.checkNow(BaseActivity.this)) {
            if (offlineFlag) {
                onFailureResponse(urlPath, NO_INTERNET);
                return false;
            } else {
                NativeUtils.noInternetAlert(BaseActivity.this);
                return false;
            }
        }
        return true;
    }

    public void callPostData(String urlPath, JSONObject jsonObject, boolean showProgress, boolean offlineFlag) {


        if (!checkInternet(urlPath, offlineFlag))
            return;
        if (showProgress)
            heptagonProgressDialog = ProgressDialog.showLoader(BaseActivity.this, false);


        RestDataHelper heptagonDataHelper = new RestDataHelper(context);
        try {
            heptagonDataHelper.postEnData(new String[]{urlPath}, jsonObject, new DataCallBack(urlPath));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        break;

                    default:
                        super.onActivityResult(requestCode, resultCode, data);
                        break;
                }
                break;
        }
    }

    private class DataCallBack implements DataCallback {
        String urlPath;

        DataCallBack(String urlPath) {
            this.urlPath = urlPath;
        }

        @Override
        public void onSuccess(String data) {
            ProgressDialog.dismissLoader(heptagonProgressDialog);

            onSuccessResponse(urlPath, data);
        }

        @Override
        public void onFailure(String error, String message) {
            ProgressDialog.dismissLoader(heptagonProgressDialog);
            onFailureResponse(urlPath, error);
        }
    }

}