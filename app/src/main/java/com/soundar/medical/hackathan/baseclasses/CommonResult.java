package com.soundar.medical.hackathan.baseclasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonResult {
    @SerializedName("Response")
    @Expose
    private String Response;

    public String getResponse() {
        return PojoUtils.checkResult(Response);
    }

    public void setResponse(String response) {
        Response = response;
    }
}
