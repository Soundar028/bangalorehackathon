package com.soundar.medical.hackathan.baseinterface;


import java.util.Map;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface RestApiService {
    @POST("{path}")
    Call<String> commonEnPost(@Path("path") String url, @QueryMap Map<String, String> fields);

}
