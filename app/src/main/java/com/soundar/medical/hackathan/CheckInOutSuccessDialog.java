package com.soundar.medical.hackathan;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

/**
 * Created by hep_android on 17/11/17.
 */

public class CheckInOutSuccessDialog extends Dialog {
    private MainActivity mainActivity;
    private String message;
    private boolean status;

    public CheckInOutSuccessDialog(@NonNull MainActivity mainActivity, String message, boolean status) {
        super(mainActivity, R.style.MyDialog_TRANSPARENT);
        this.mainActivity = mainActivity;
        this.message = message;
        this.status = status;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (getWindow() != null)
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setContentView(R.layout.dialog_check_in_out_success);

        final LottieAnimationView lav_animation = findViewById(R.id.lav_animation);
        final LottieAnimationView lav_error_animation = findViewById(R.id.lav_error_animation);
        TextView tv_status = findViewById(R.id.tv_status);

        if (status)
            lav_animation.setVisibility(View.VISIBLE);
        else
            lav_error_animation.setVisibility(View.VISIBLE);

        lav_animation.playAnimation();
        lav_error_animation.playAnimation();

        tv_status.setText(message);

        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f).setDuration(1500);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                lav_animation.setProgress((Float) valueAnimator.getAnimatedValue());
            }
        });
        animator.start();

        setTitle("");
        initViews();
    }

    private void initViews() {


    }

    public void closeDialog() {
        dismiss();
    }
}
