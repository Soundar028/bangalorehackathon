package com.soundar.medical.hackathan;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.soundar.medical.hackathan.baseactivity.BaseActivity;
import com.soundar.medical.hackathan.baseclasses.NativeUtils;
import com.soundar.medical.hackathan.baseclasses.PreferenceManager;
import com.soundar.medical.hackathan.baseclasses.TextConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends BaseActivity {

    private final int REQ_CODE_SPEECH_INPUT = 100;
    CheckInOutSuccessDialog checkInOutSuccessDialog;
    String message = "";
    private TextView txtSpeechInput;
    private ImageButton btnSpeak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = this.getApplicationContext().getSharedPreferences(TextConstant.PREF_NAME, Context.MODE_PRIVATE);
        PreferenceManager.initializePreferenceManager(sharedPreferences);
        PreferenceManager.setString(TextConstant.APP_DOMAIN, "http://inedgedev.heptagon.tech/");

        txtSpeechInput = findViewById(R.id.txtSpeechInput);
        btnSpeak = findViewById(R.id.btnSpeak);

        btnSpeak.setOnClickListener(v -> promptSpeechInput());
    }

    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccessResponse(String key, String data) {
        if (key.equals(TextConstant.APP_TEXT_SEND)) {
            LoginResponse result = NativeUtils.jsonToPojoParser(data, LoginResponse.class);
            if (result != null) {
                if (result.getStatus()) {
                    message = result.getMessage();
                    checkInOutSuccessDialog = new CheckInOutSuccessDialog(MainActivity.this, message, true);
                    checkInOutSuccessDialog.show();
                } else {
                    message = result.getMessage();
                    checkInOutSuccessDialog = new CheckInOutSuccessDialog(MainActivity.this, message, false);
                    checkInOutSuccessDialog.show();
                }
            }
        }
    }

    @Override
    public void onFailureResponse(String key, String error) {

    }

    @Override
    protected void initViews() {

    }

    /**
     * Receiving speech input
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("input", result.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    callPostData(TextConstant.APP_TEXT_SEND, jsonObject, true, false);
                }
                break;
            }

        }
    }

}
